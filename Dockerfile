FROM nginx:alpine as nginx 
FROM php:7.2.1-fpm-alpine as php

COPY --from=nginx / /
COPY --from=php / /
# 复制配置文件
COPY / /

EXPOSE 80 443

CMD "php-fpm && nginx -g 'daemon off;'"